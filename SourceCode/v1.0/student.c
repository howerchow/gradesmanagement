#include "student.h"
#include <inttypes.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void menu(void)
{
        system("clear");
        printf("\t\t欢迎使用学生成绩管理系统\n");
        printf("********************菜单******************************\n");
        printf("\t1 成绩添加\t\t 2 成绩删除\n");
        printf("\t3 成绩查询\t\t 4 成绩统计\n");
        printf("\t5 信息修改\t\t 6 成绩打印\n");
        printf("\t0 显示菜单\t\t 9 程序退出\n");
        printf("******************************************************\n");
}

void student_print(Student stu)
{
        printf("---------------------------------------------------------------------------\n");
        printf("学号\t姓名\t性别\t年龄\t英语\t\t语文\t\t数学\n");
        printf("%" PRIu16 "\t%s\t%c\t%" PRId8 "\t%f\t%f\t%f\n", stu.no, stu.name, stu.sex, stu.age, stu.english,
               stu.chinese, stu.math);
        printf("---------------------------------------------------------------------------\n");
}

void student_array_print(Student *stu, size_t len)
{
        printf("\t\t学生成绩信息\n");
        printf("---------------------------------------------------------------------------\n");
        printf("学号\t姓名\t性别\t年龄\t英语\t\t语文\t\t数学\n");
        for (size_t i = 0; i < len; ++i)
                printf("%" PRIu16 "\t%s\t%c\t%" PRId8 "\t%f\t%f\t%f\n", stu[i].no, stu[i].name, stu[i].sex, stu[i].age,
                       stu[i].english, stu[i].chinese, stu[i].math);
        printf("---------------------------------------------------------------------------\n");
}

int student_array_init(Student *stu, size_t *len)
{
        size_t i = 0;
        FILE *fp = fopen("student.dat", "ab+");
        if (fp == NULL)
        {
                printf("文件打开失败!\n");
                return -1;
        }
        fread(&stu[i], sizeof(Student), 1, fp);
        while (!feof(fp))
        {
                fread(&stu[++i], sizeof(Student), 1, fp);
                if (ferror(fp))
                {
                        printf("读取文件失败!\n");
                        return -1;
                }
        }

        *len = i;
        fclose(fp);
        return 0;
}

int student_array_save(Student *stu, size_t len)
{
        FILE *fp;
        if ((fp = fopen("student.dat", "wb")) == NULL)
        {
                printf("文件打开失败!\n");
                return -1;
        }
        for (size_t i = 0; i < len; ++i)
        {
                fwrite(&stu[i], sizeof(Student), 1, fp);
                if (ferror(fp))
                {
                        printf("数据写入文件失败!\n");
                        return -1;
                }
        }

        fclose(fp);
        return 0;
}

int student_add(Student *stu, size_t *len)
{
        Student std;
        int num, i;
        printf("请输入要录入的学生人数:");
        scanf("%d", &num);
        for (i = 0; i < num; ++i)
        {
                printf("请输入第 %d 个学生的信息\n", i + 1);
                printf("学号(1000):");
                scanf(" %" SCNu16 "", &std.no);
                printf("姓名:");
                scanf(" %s", std.name);
                printf("性别:");
                scanf(" %c", &std.sex);
                printf("年龄:");
                scanf(" %" SCNd8 "", &std.age);
                printf("英语成绩:");
                scanf(" %f", &std.english);
                printf("语文成绩:");
                scanf(" %f", &std.chinese);
                printf("数学成绩:");
                scanf(" %f", &std.math);
                stu[*len] = std;
                ++(*len);
        }
        student_array_save(stu, *len);
        student_array_print(stu, *len);
        return 0;
}

static int student_del_by_name(Student *stu, size_t *len, char *name)
{
        size_t i = 0;
        while (i < *len)
        {
                if (strcmp(name, stu[i].name) == 0)
                        break;
                ++i;
        }

        if (i == *len)
        {
                printf("删除失败,无法找到学生 %s的信息!\n", name);
                return -1;
        }

        while (i < *len - 1)
        {
                stu[i] = stu[i + 1];
                ++i;
        }

        --(*len);

        return 0;
}

int student_del_by_no(Student *stu, size_t *len, uint16_t no)
{
        size_t i = 0;
        while (i < *len)
        {
                if (stu[i].no == no)
                        break;
                ++i;
        }

        if (i == *len)
        {
                printf("删除失败,无法找到学号 %d 的信息!\n", no);
                return -1;
        }

        while (i < *len - 1)
        {
                stu[i] = stu[i + 1];
                ++i;
        }
        --(*len);

        return 0;
}

int student_delete(Student *stu, size_t *len)
{
        student_array_print(stu, *len);
        printf("--------------------\n");
        printf("1:通过姓名查找删除\n");
        printf("2:通过学号查找删除\n");
        printf("--------------------\n");
        int select;
        printf("请输入删除选项:");
        scanf("%d", &select);
        if (select == 1)
        {
                char name[20];
                printf("请输入要删除的学生姓名:");
                scanf("%s", name);
                student_del_by_name(stu, len, name);
        }
        else if (select == 2)
        {
                uint16_t no;
                printf("请输入要删除的学生学号:");
                scanf("%" SCNu16 "", &no);
                student_del_by_no(stu, len, no);
        }
        else
        {
                printf("无效输入!\n");
                return -1;
        }
        student_array_save(stu, *len);
        student_array_print(stu, *len);
        return 0;
}

static Student stu_find_by_name(Student *stu, size_t len, char *name)
{
        int i;
        for (i = 0; i < len; i++)
        {
                if (strcmp(name, stu[i].name) == 0)
                        break;
        }
        if (i == len)
        {
                printf("查找失败!\n无法查找到学生 %s 的信息!\n", name);
                return stu[0];
        }
        printf("查找成功，该学生信息:\n");
        student_print(stu[i]);
        return stu[i];
}

static Student stu_find_by_no(Student *stu, size_t len, uint16_t no)
{
        size_t i;
        for (i = 0; i < len; ++i)
        {
                if (stu[i].no == no)
                        break;
        }
        if (i == len)
        {
                printf("查找失败!\n无法查找到学号 %d 的信息!\n", no);
                return stu[0];
        }
        printf("查找成功，该学生信息:\n");
        student_print(stu[i]);
        return stu[i];
}

Student student_find(Student *stu, size_t len)
{
        int select;
        printf("--------------------------\n");
        printf("1:通过姓名查找\n");
        printf("2:通过学号查找\n");
        printf("--------------------------\n");
        printf("请输入查找选项:");
        scanf("%d", &select);
        if (select == 1)
        {
                char name[20];
                printf("请输入要查找的学生姓名:");
                scanf("%s", name);
                stu_find_by_name(stu, len, name);
        }
        else if (select == 2)
        {
                uint16_t no;
                printf("请输入要查找的学生学号:");
                scanf("%" SCNu16 "", &no);
                stu_find_by_no(stu, len, no);
        }
        else
        {
                printf("invalid input!\n");
                return stu[0];
        }
}

int student_tongji(Student *stu, size_t len)
{
        float chinese_avr = 0, english_avr = 0, math_avr = 0;
        int chinese_num = 0, english_num = 0, math_num = 0;
        int total_num = 0;
        float score_avr[len];
        float avr = 0;
        for (size_t i = 0; i < len; i++)
        {
                chinese_avr += stu[i].chinese;
                english_avr += stu[i].english;
                math_avr += stu[i].math;
                score_avr[i] = stu[i].chinese + stu[i].english + stu[i].math;
                score_avr[i] /= 3;
                if (stu[i].chinese < 60)
                        chinese_num++;
                if (stu[i].english < 60)
                        english_num++;
                if (stu[i].math < 60)
                        math_num++;
        }

        chinese_avr /= len;
        english_avr /= len;
        math_avr /= len;
        total_num = chinese_num + english_num + math_num;
        avr = (chinese_avr + english_avr + math_avr) / 3;

        printf("\t\t\t学生成绩统计\n");
        printf("-------------------------------------------------------------\n");
        printf("学号\t姓名\t平均分\n");
        for (size_t i = 0; i < len; ++i)
                printf("%" PRIu16 "\t%s\t%f\n", stu[i].no, stu[i].name, score_avr[i]);
        printf("-------------------------------------------------------------\n");
        printf("语文平均成绩: %f\t\t不及格人数: %d\n", chinese_avr, chinese_num);
        printf("英语平均成绩: %f\t\t不及格人数: %d\n", english_avr, english_num);
        printf("数学平均成绩: %f\t\t不及格人数: %d\n", math_avr, math_num);
        printf("-------------------------------------------------------------\n");
        printf("总体平均分：%f\n", avr);
        printf("总体不及格人数:%d\n", total_num);
        printf("-------------------------------------------------------------\n");

        return 0;
}

static int stu_modify_by_name(Student *stu, int len, char *name)
{
        int i;
        for (i = 0; i < len; i++)
        {
                if (strcmp(name, stu[i].name) == 0)
                        break;
        }
        if (i == len)
        {
                printf("查找失败!\n没有找到学生 %s 的信息!\n", name);
                return -1;
        }

        printf("-----------------------------\n");
        printf("1:修改学号\n");
        printf("2:修改姓名\n");
        printf("3:修改性别\n");
        printf("4:修改年龄\n");
        printf("5:修改语文成绩\n");
        printf("6:修改英语成绩\n");
        printf("7:修改数学成绩\n");
        printf("------------------------------\n");

        printf("请输入修改选项:");
        int select;
        scanf(" %d", &select);
        switch (select)
        {
        case 1:
                printf("输入新学号:");
                scanf(" %" SCNu16 "", &stu[i].no);
                break;
        case 2:
                printf("输入新姓名:");
                scanf(" %s", stu[i].name);
                break;
        case 3:
                printf("输入新性别:");
                scanf(" %c", &stu[i].sex);
                break;
        case 4:
                printf("输入新年龄:");
                scanf(" %" SCNu8 "", &stu[i].age);
                break;
        case 5:
                printf("输入语文成绩:");
                scanf(" %f", &stu[i].chinese);
                break;
        case 6:
                printf("输入英语成绩:");
                scanf(" %f", &stu[i].english);
                break;
        case 7:
                printf("输入数学成绩:");
                scanf(" %f", &stu[i].math);
                break;
        default:
                printf("错误输入!\n");
                break;
        }

        student_array_save(stu, len);
        student_array_print(stu, len);
}

static int stu_modify_by_no(Student *stu, size_t len, uint16_t no)
{
        int i;
        for (i = 0; i < len; i++)
        {
                if (no == stu[i].no)
                        break;
        }
        if (i == len)
        {
                printf("查找失败!\n没有找到学号 %d 的信息!\n", no);
                return -1;
        }

        printf("-----------------------------\n");
        printf("1:修改学号\n");
        printf("2:修改姓名\n");
        printf("3:修改性别\n");
        printf("4:修改年龄\n");
        printf("5:修改语文成绩\n");
        printf("6:修改英语成绩\n");
        printf("7:修改数学成绩\n");
        printf("------------------------------\n");

        printf("请输入修改选项:");
        int select;
        scanf(" %d", &select);
        switch (select)
        {
        case 1:
                printf("输入新的学号:");
                scanf(" %" SCNu16 "", &stu[i].no);
                break;
        case 2:
                printf("输入新的姓名:");
                scanf(" %s", stu[i].name);
                break;
        case 3:
                printf("输入性别:");
                scanf(" %c", &stu[i].sex);
                break;
        case 4:
                printf("输入年龄:");
                scanf(" %" SCNu8 "", &stu[i].age);
                break;
        case 5:
                printf("输入语文成绩:");
                scanf(" %f", &stu[i].chinese);
                break;
        case 6:
                printf("输入英语成绩:");
                scanf(" %f", &stu[i].english);
                break;
        case 7:
                printf("输入数学成绩:");
                scanf(" %f", &stu[i].math);
                break;
        default:
                printf("错误的输入!\n");
                break;
        }

        student_array_save(stu, len);
        student_array_print(stu, len);
}

int student_modify(Student *stu, size_t len)
{
        int select;
        printf("--------------------------\n");
        printf("1:通过姓名查找修改\n");
        printf("2:通过学号查找修改\n");
        printf("--------------------------\n");
        printf("请输入修改选项:");
        scanf("%d", &select);
        if (select == 1)
        {
                char name[20];
                printf("请输入要查找的学生姓名:");
                scanf("%s", name);
                stu_modify_by_name(stu, len, name);
        }
        else if (select == 2)
        {
                int no;
                printf("请输入要查找的学生学号:");
                scanf("%d", &no);
                stu_modify_by_no(stu, len, no);
        }
        else
        {
                printf("错误输入!\n");
                return -1;
        }
}
