#pragma once

#include <stddef.h>
#include <stdint.h>
#include <stdio.h>

typedef struct student
{
        uint16_t no;
        char name[10];
        char sex;
        int8_t age;
        float english;
        float chinese;
        float math;

} Student;

void menu(void);
void student_print(Student stu);
void student_array_print(Student *stu, size_t len);
int student_array_init(Student *stu, size_t *len);
int student_array_save(Student *stu, size_t len);
int student_add(Student *stu, size_t *len);

int student_delete(Student *stu, size_t *len);
Student student_find(Student *stu, size_t len);

int student_tongji(Student *stu, size_t len);
int student_modify(Student *stu, size_t len);
