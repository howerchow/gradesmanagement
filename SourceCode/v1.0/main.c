#include "student.h"
#include <stdio.h>
#include <stdlib.h>

size_t actual_len = 0;
Student stu[50];

int main()
{
        int i, choice;
        student_array_init(stu, &actual_len);
        menu();
        while (1)
        {

                printf("请输入操作选项(0~9):");
                scanf("%d", &choice);
                system("clear");
                menu();
                int no;
                switch (choice)
                {
                case 0:
                        system("clear");
                        menu();
                        break;
                case 1:
                        student_add(stu, &actual_len);
                        break;
                case 2:
                        student_delete(stu, &actual_len);
                        break;
                case 3:
                        student_find(stu, actual_len);
                        break;
                case 4:
                        student_tongji(stu, actual_len);
                        break;
                case 5:
                        student_modify(stu, actual_len);
                        break;
                case 6:
                        student_array_print(stu, actual_len);
                        break;
                case 9:
                        system("clear");
                        return 0;
                        break;
                default:
                        printf("无效输入!\n");
                        break;
                }
        }

        return 0;
}
