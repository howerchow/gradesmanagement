/*
软件功能描述：
    实现C语言成绩管理系统：成绩的添加、删除、
    修改、排序 、查询、存储、打印等
*/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "student_list.h"

int main()
{
        StudentList *slist = malloc(sizeof(StudentList));
        memset(slist, 0, sizeof(StudentList));
        student_list_init(slist);

        main_menu();
        int choice;
        while (1)
        {

                printf("please input your choice(0~9):");
                scanf("%d", &choice);
                system("clear");
                main_menu();
                int no;
                switch (choice)
                {
                case 0:
                        system("clear");
                        main_menu();
                        break;
                case 1:
                        student_add(slist);
                        break;
                case 2:
                        student_delete(slist);
                        break;
                case 3:
                        student_find(slist);
                        break;
                case 4:
                        student_tongji(slist);
                        break;
                case 5:
                        student_modify(slist);
                        break;
                case 6:
                        student_list_print(slist);
                        break;
                case 7:
                        student_sort(slist);
                        break;
                case 8:
                        // student_set_color();
                        break;
                case 9:
                        system("clear");
                        student_list_destroy(slist);
                        return 0;
                        break;
                default:
                        printf("error input!\n");
                        break;
                }
        }

        return 0;
}
