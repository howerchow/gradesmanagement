#include "student_list.h"
#include <inttypes.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void main_menu(void)
{
        system("clear");
        printf("\t\tWellcome To Student Scores  Management System\n");
        printf("**********************Menu******************************\n");
        printf("\t1 Score Add\t\t 2 Score Delete\n");
        printf("\t3 Score Query\t\t 4 Score Statistics\n");
        printf("\t5 Info Modify\t\t 6 Score Print\n");
        printf("\t7 Score Sort\t\t 8 Software Configure\n");
        printf("\t0 Show Menu\t\t 9 System Exit\n");
        printf("******************************************************\n");
}

static void student_print(StudentNode *q)
{
        printf("\t\tStudent Score Info\n");
        printf("------------------------------------------------------------------------\n");
        printf("no\tname\tsex\tage\tEnglish\t\tChinese\t\tMath\n");

        printf("%" PRIu16 "\t%s\t%c\t%" PRIu8 "\t%f\t%f\t%f\n", q->data.no, q->data.name, q->data.sex, q->data.age,
               q->data.english, q->data.chinese, q->data.math);

        printf("------------------------------------------------------------------------\n");
}

StudentList *student_list_create()
{
        StudentList *list;

        if ((list = malloc(sizeof(StudentList))) == NULL)
                return NULL;
        list->head = list->tail = NULL;
        list->len = 0;
        return list;
}

int student_list_init(StudentList *slist)
{
        FILE *fp;
        size_t num;
        StudentNode *p;

        StudentNode *q = NULL;
        if ((fp = fopen("student.dat", "ab+")) == NULL)
        {
                printf("open file failed!\n");
                return -1;
        }

        p = malloc(sizeof(StudentNode));
        slist->head = p;
        slist->tail = p;
        slist->len = 1;
        num = fread(&(p->data), sizeof(Student), 1, fp);
        p->next = NULL;

        if (num == 0)
        {
                slist->head = NULL;
                slist->tail = NULL;
                free(p);
                return 0;
        }

        while (!feof(fp))
        {
                q = malloc(sizeof(StudentNode));
                memset(q, 0, sizeof(StudentNode));
                num = fread(&(q->data), sizeof(Student), 1, fp);
                if (ferror(fp))
                {
                        printf("read file failed!\n");
                        fclose(fp);
                        return -4;
                }
                q->next = NULL;

                if (num == 0)
                {
                        free(q);
                        break;
                }
                p->next = q;
                p = q;
                p->next = NULL;
                ++(slist->len);
        }
        slist->tail = p;
        fclose(fp);
        return 0;
}

int student_list_destroy(StudentList *slist)
{
        if (slist == NULL)
        {
                return 0;
        }
        if (slist->head == NULL)
        {
                free(slist);
                return 0;
        }

        StudentNode **p = &slist->head;
        while (*p != NULL)
        {
                StudentNode *t = *p;
                p = &(*p)->next;
                free(t);
        };

        free(slist);

        return 0;
}

int student_list_save(StudentList *slist)
{
        FILE *fp;
        StudentNode *p = NULL;

        if ((fp = fopen("student.dat", "wb")) == NULL)
        {
                printf("open file failed!\n");
                return -1;
        }
        p = slist->head;
        while (p != NULL)
        {
                fwrite(&(p->data), sizeof(Student), 1, fp);
                if (ferror(fp))
                {
                        printf("write data failed!\n");
                        fclose(fp);
                        return -2;
                }
                p = p->next;
        }

        fclose(fp);
        return 0;
}

void student_list_print(StudentList *slist)
{
        printf("\t\tStudent Score Info\n");
        printf("------------------------------------------------------------------------\n");
        printf("no\tname\tsex\tage\tEnglish\t\tChinese\t\tMath\n");
        StudentNode *snode = slist->head;
        do
        {
                printf("%" PRIu16 "\t%s\t%c\t%" PRIu8 "\t%f\t%f\t%f\n", snode->data.no, snode->data.name,
                       snode->data.sex, snode->data.age, snode->data.english, snode->data.chinese, snode->data.math);
                snode = snode->next;
        } while (snode != NULL);

        printf("------------------------------------------------------------------------\n");
}

int student_add(StudentList *slist)
{
        StudentNode *q;
        q = malloc(sizeof(StudentNode));
        memset(q, 0, sizeof(StudentNode));
        printf("请输入要添加的学生信息\n");
        printf("输入学号:");
        scanf(" %" SCNu16 "", &q->data.no);
        printf("输入姓名:");
        scanf(" %s", q->data.name);
        printf("输入性别:");
        fflush(stdin);
        scanf(" %c", &q->data.sex);
        printf("输入年龄:");
        scanf(" %" SCNu8 "", &q->data.age);
        printf("输入英语:");
        scanf(" %f", &q->data.english);
        printf("输入语文:");
        scanf(" %f", &q->data.chinese);
        printf("输入数学:");
        scanf(" %f", &q->data.math);
        slist->tail->next = q;
        q->next = NULL;
        ++(slist->len);
        student_list_save(slist);
        return 0;
}

static int student_delete_by_no(StudentList *slist, uint16_t no)
{
        StudentNode **Pre = &slist->head;
        while (*Pre != NULL)
        {
                StudentNode *Entry = *Pre;

                if (Entry->data.no == no)
                {
                        *Pre = Entry->next;
                        free(Entry);
                }
                else
                {
                        Pre = &Entry->next;
                }
        }

        return 0;
}

int student_delete(StudentList *slist)
{
        printf("-------------------------------\n");
        uint16_t no;
        printf("input no:");
        scanf(" %" SCNu16 "", &no);
        student_delete_by_no(slist, no);
        student_list_save(slist);
        return 0;
}

int student_find_by_name(StudentList *slist, char *name)
{
        StudentNode *p = slist->head;
        while (p != NULL)
        {
                if (strcmp(name, p->data.name) == 0)
                        break;
                p = p->next;
        }
        if (p == NULL)
        {
                printf("find %s failed!\n", name);
                return -1;
        }
        student_print(p);
        return 0;
}

int student_find_by_no(StudentList *slist, uint16_t no)
{
        StudentNode *p = slist->head;
        while (p != NULL)
        {
                if (no == p->data.no)
                        break;
                p = p->next;
        }
        if (p == NULL)
        {
                printf("find %d failed!\n", no);
                return -1;
        }
        student_print(p);
        return 0;
}

int student_find(StudentList *slist)
{
        printf("-------------------------------\n");
        printf("1:find by name\n");
        printf("2:find by no\n");
        printf("-------------------------------\n");
        int select;
        printf("input choice:");
        scanf("%d", &select);
        if (select == 1)
        {
                char name[20];
                printf("input name:");
                scanf("%s", name);
                student_find_by_name(slist, name);
        }

        else if (select == 2)
        {
                unsigned int no;
                printf("input no:");
                scanf("%d", &no);
                student_find_by_no(slist, no);
        }
        else
        {
                printf("invalid input!\n");
                return -1;
        }

        return 0;
}

int student_node_modify(StudentNode *p)
{
        printf("------------------------------------\n");
        printf("1.modify no\n");
        printf("2.modify name\n");
        printf("3.modify sex\n");
        printf("4.modify age\n");
        printf("5.modify english score\n");
        printf("6.modify chinese score\n");
        printf("7.modify math score\n");
        printf("------------------------------------\n");
        int select;
        printf("input your choice:");
        scanf("%d", &select);
        switch (select)
        {
        case 1:
                printf("input new no:");
                scanf("%" SCNu16 "", &p->data.no);
                break;
        case 2:
                printf("input new name:");
                scanf("%s", p->data.name);
                break;
        case 3:
                printf("input new sex:");
                fflush(stdin);
                scanf("%c", &p->data.sex);
                break;
        case 4:
                printf("input new age:");
                scanf("%" SCNu8 "", &p->data.age);
                break;
        case 5:
                printf("input new English score:");
                scanf("%f", &p->data.english);
                break;
        case 6:
                printf("input new Chinese score:");
                scanf("%f", &p->data.chinese);
                break;
        case 7:
                printf("input new Math score:");
                scanf("%f", &p->data.math);
                break;
        default:
                printf("invalid input!\n");
                break;
        }
        return 0;
}

int student_modify_by_name(StudentList *slist, char *name)
{
        StudentNode *p = slist->head;
        while (p != NULL)
        {
                if (strcmp(name, p->data.name) == 0)
                        break;
                p = p->next;
        }
        if (p == NULL)
        {
                printf("find %s failed!\n", name);
                return -1;
        }
        student_node_modify(p);
        student_print(p);
        return 0;
}

int student_modify_by_no(StudentList *slist, uint16_t no)
{
        StudentNode *p = slist->head;
        while (p != NULL)
        {
                if (no == p->data.no)
                        break;
                p = p->next;
        }
        if (p == NULL)
        {
                printf("find %d failed!\n", no);
                return -1;
        }
        student_node_modify(p);
        student_print(p);
        return 0;
}

int student_modify(StudentList *slist)
{
        printf("-------------------------------\n");
        printf("1:modify by name\n");
        printf("2:modify by no\n");
        printf("-------------------------------\n");
        int select;
        printf("input choice:");
        scanf("%d", &select);
        if (select == 1)
        {
                char name[20];
                printf("input name:");
                scanf("%s", name);
                student_modify_by_name(slist, name);
        }

        else if (select == 2)
        {
                unsigned int no;
                printf("input no:");
                scanf("%d", &no);
                student_modify_by_no(slist, no);
        }
        else
        {
                printf("invalid input!\n");
                return -1;
        }
        student_list_save(slist);
        return 0;
}

int student_tongji(StudentList *slist)
{
        float chinese_avr = 0, english_avr = 0, math_avr = 0;
        int chinese_num = 0, english_num = 0, math_num = 0;
        int total_num = 0;
        int len = slist->len;
        StudentNode *p = NULL;
        p = slist->head;

        float score_avr[len + 1];
        float avr = 0;

        int i = 0;
        while (p != NULL)
        {
                chinese_avr += p->data.chinese;
                english_avr += p->data.english;
                math_avr += p->data.math;
                score_avr[i] = p->data.chinese + p->data.english + p->data.math;
                score_avr[i] /= 3;
                if (p->data.chinese < 60)
                        chinese_num++;
                if (p->data.english < 60)
                        english_num++;
                if (p->data.math < 60)
                        math_num++;
                ++i;
                p = p->next;
        }

        chinese_avr /= len;
        english_avr /= len;
        math_avr /= len;
        total_num = chinese_num + english_num + math_num;
        avr = (chinese_avr + english_avr + math_avr) / 3;

        printf("\t\t学生成绩统计\n");
        printf("---------------------------------\n");
        printf("学号\t姓名\t平均分\n");
        p = slist->head;
        for (i = 0; p != NULL; ++i, p = p->next)
                printf("%d\t%s\t%f\n", p->data.no, p->data.name, score_avr[i]);
        printf("-------------------------------------------------------------\n");
        printf("语文平均成绩: %f\t不及格人数: %d\n", chinese_avr, chinese_num);
        printf("英语平均成绩: %f\t不及格人数: %d\n", english_avr, english_num);
        printf("数学平均成绩: %f\t不及格人数: %d\n", math_avr, math_num);
        printf("-------------------------------------------------------------\n");
        return 0;
}

static void sort_menu()
{
        printf("--------------------------------\n");
        printf("1.sort by no\n");
        printf("2.sort by name\n");
        printf("3.sort by age\n");
        printf("4.sort by english score\n");
        printf("5.sort by chinese score\n");
        printf("6.sort by math score\n");
        printf("7.sort by total score\n");
        printf("--------------------------------\n");
}

static int swap(StudentNode *p, StudentNode *q, int select)
{
        Student tmp;
        switch (select)
        {
        case 1:
                if (p->data.no > q->data.no)
                {
                        tmp = p->data;
                        p->data = q->data;
                        q->data = tmp;
                }
                break;
        case 2:
                if (strcmp(p->data.name, q->data.name) > 0)
                {
                        tmp = p->data;
                        p->data = q->data;
                        q->data = tmp;
                }
                break;
        case 3:
                if (p->data.age > q->data.age)
                {
                        tmp = p->data;
                        p->data = q->data;
                        q->data = tmp;
                }
                break;
        case 4:
                if (p->data.english < q->data.english)
                {
                        tmp = p->data;
                        p->data = q->data;
                        q->data = tmp;
                }
                break;
        case 5:
                if (p->data.chinese < q->data.chinese)
                {
                        tmp = p->data;
                        p->data = q->data;
                        q->data = tmp;
                }
                break;
        case 6:
                if (p->data.math < q->data.math)
                {
                        tmp = p->data;
                        p->data = q->data;
                        q->data = tmp;
                }
                break;
        case 7:
                if (p->data.english + p->data.chinese + p->data.math < q->data.english + q->data.chinese + q->data.math)
                {
                        tmp = p->data;
                        p->data = q->data;
                        q->data = tmp;
                }
                break;
        default:
                printf("invalid input!\n");
                return -1;
                break;
        }
        return 0;
}

int student_sort(StudentList *slist)
{
        StudentNode *p, *q;
        Student tmp;
        sort_menu();
        int select;
        printf("input your choice:");
        scanf("%d", &select);
        for (p = slist->head; p->next != NULL; p = p->next)
        {
                for (q = p->next; q != NULL; q = q->next)
                        swap(p, q, select);
        }
        student_list_print(slist);
        return 0;
}
