#pragma once

#include <stddef.h>
#include <stdint.h>

typedef struct Student
{
        uint16_t no;
        char name[10];
        char sex;
        uint8_t age;
        float english;
        float chinese;
        float math;
} Student;

typedef struct StudentNode
{
        Student data;
        struct StudentNode *next;
} StudentNode;

typedef struct StudentList
{
        StudentNode *head;
        StudentNode *tail;
        uint64_t len;
} StudentList;

void main_menu(void);

void student_list_print(StudentList *slist);

int student_list_init(StudentList *slist);
int student_list_destroy(StudentList *slist);

int student_list_save(StudentList *slist);

int student_add(StudentList *slist);

int student_delete(StudentList *slist);

int student_find(StudentList *slist);

int student_modify(StudentList *slist);


int student_tongji(StudentList *slist);

int student_sort(StudentList *slist);
